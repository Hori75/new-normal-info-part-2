from django.test import TestCase
from django.test import Client
from django.urls import reverse
from .models import RS, City
from django.contrib.auth.models import User
from django.test.client import RequestFactory

# Create your tests here.

class Testing123(TestCase):
    def test_url_home_ada(self):
        response = Client().get(reverse('daftarrumahsakit:home'))
        self.assertEquals(response.status_code, 200)

    def test_elemen_inti_dan_template_home_ada(self):
        City.objects.create(name="aaa", city_slug = "bbb", img = "ccc")
        response = Client().get(reverse('daftarrumahsakit:home'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Rumah Sakit Rujukan", html_kembalian)
        self.assertIn("COVID-19 DKI Jakarta", html_kembalian)
        self.assertIn("RS/Kota/Kelurahan", html_kembalian)
        self.assertIn("Search", html_kembalian)
        self.assertIn("aaa", html_kembalian)
        self.assertIn("aaa", html_kembalian)
        self.assertIn("Login untuk mendapatkan", html_kembalian)
        self.assertIn("daftar rumah sakit rujukan COVID-19 di Indonesia!", html_kembalian)
        self.assertTemplateUsed(response, 'daftarrumahsakit/home.html')

    def test_url_dan_template_search_ada(self):
        response = Client().get(reverse('daftarrumahsakit:search'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'daftarrumahsakit/search.html')

        html_kembalian = response.content.decode('utf8')
        self.assertIn("Rumah Sakit Rujukan", html_kembalian)
        self.assertIn("COVID-19 DKI Jakarta", html_kembalian)
        self.assertIn("RS/Kota/Kelurahan", html_kembalian)
        self.assertIn("Search", html_kembalian)

    def test_modul_berfungsi_dan_url_slug(self):
        jakarta = City.objects.create(name="aaa", city_slug = "bbb", img = "ccc")
        total1 = City.objects.all().count()
        self.assertEquals(total1, 1)

        response = Client().get(reverse('daftarrumahsakit:single_slug', args=['bbb']))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'daftarrumahsakit/detail.html')

        RS.objects.create(name="ddd", loc = "eee", contact = "fff", city = jakarta)
        total2 = RS.objects.all().count()
        self.assertEquals(total2, 1)

        html_kembalian = response.content.decode('utf8')
        self.assertIn("Rumah Sakit Rujukan", html_kembalian)
        self.assertIn("COVID-19 DKI Jakarta", html_kembalian)
        self.assertIn("RS/Kota/Kelurahan", html_kembalian)
        self.assertIn("Search", html_kembalian)
        self.assertIn("Please login to get the info", html_kembalian)

    def test_query(self):
        jakarta = City.objects.create(name="aaa", city_slug = "bbb", img = "ccc")
        RS.objects.create(name="111", loc = "222", contact = "333", city = jakarta)
        RS.objects.create(name="444", loc = "555", contact = "666", city = jakarta)
        RS.objects.create(name="777", loc = "888", contact = "999", city = jakarta)

        response = self.client.post(reverse('daftarrumahsakit:search'),data={
            "srh" : "111"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "daftarrumahsakit/search.html")
        



        

