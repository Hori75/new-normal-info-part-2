from django.contrib import admin
from django.urls import path

from .views import index, Subscribe, SaveSubscription, developer

app_name = "main"

urlpatterns = [
    path('', index, name="index"),
    path('subscribe/', Subscribe, name="subscribe"),
    path('subscribe/save/', SaveSubscription, name="save"),
    path('developer/', developer, name="developer")
]