from django.contrib import admin
from django.urls import path

from . import views

app_name = "forum"

urlpatterns = [
    path('', views.index, name="index"),
    path('getposts', views.getPosts, name="getPosts"),
    path('view/<int:id>', views.view, name="view"),
    path('post/', views.post, name="post"),
    path('reply/<int:id>', views.reply, name="reply"),
    path('upvote/<int:id>', views.upvote, name="upvote"),
    path('downvote/<int:id>', views.downvote, name="downvote")
]