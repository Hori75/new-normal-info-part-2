# Generated by Django 3.1.2 on 2020-12-30 13:41

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0004_merge_20201120_2243'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='downvoters',
            field=models.ManyToManyField(related_name='downvoter', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='topic',
            name='upvoters',
            field=models.ManyToManyField(related_name='upvoter', to=settings.AUTH_USER_MODEL),
        ),
    ]
