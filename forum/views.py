from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotFound, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.utils.html import escape

from .models import PostNode, PostNodeForm, Topic, TopicForm

def index(request, ordering="recent"):
    return render(request, "forum/index.html")

def getPosts(request):
    search = request.GET.get("search") or ""
    orderBy = request.GET.get("sort-by") or ""
    page = request.GET.get("p")
    # page argument check
    if (page == None or int(page) <= 0):
        if (orderBy != "popular" and orderBy != "recent"):
            return redirect(reverse("forum:getPosts")+"?sort-by=recent&p=1")
        else:
            return redirect(reverse("forum:getPosts")+"?sort-by="+orderBy+"&p=1")
    else:
        page = int(page)
    # orderBy argument check
    if (orderBy == "popular"):
        topics = Topic.objects.all().order_by("-score")
    elif (orderBy == "recent"):
        topics = Topic.objects.all().order_by("-recent_update")
    else: 
        return redirect(reverse("forum:getPosts")+"?sort-by=recent&p="+str(page))
    # Search query
    if (search != ""):
        topics = topics.filter(title__contains=search)
    # check if the page requested is available
    total_topics = len(topics)
    if (total_topics < (page-1)*10):
        page = total_topics // 10 + 1
        if (search != ""):
            return redirect(reverse("forum:getPosts")+"?sort-by="+orderBy+"&p="+str(page)+"&search="+search)
        else:
            return redirect(reverse("forum:getPosts")+"?sort-by="+orderBy+"&p="+str(page))
    else:
        topics = topics[(page-1)*10 : page*10]
    # Deserialization
    contentJson = dict()
    i = 0
    for topic in topics:
        post_pk = topic.pk
        post = {
            "title" : escape(topic.title),
            "recent_update" : topic.get_recent_datetime(),
            "reply_count" : topic.reply_count,
            "score" : str(topic.score),
            "post_pk" : str(post_pk)
        }
        if (topic.author != None):
            post["author"] = topic.author.username
        if (request.user.is_authenticated):
            if (request.user in topic.upvoters.all()):
                post["vote"] = "up"
            elif (request.user in topic.downvoters.all()):
                post["vote"] = "down"
            else:
                post["vote"] = "none"
        else:
            post["vote"] = "none"
        contentJson[i] = post
        i += 1

    responseJson = {
        "currPage" : str(page),
        "totalPages" : str((total_topics // 10) + 1),
        "content" : contentJson
    }

    return JsonResponse(responseJson)
    

def view(request, id):
    topic = Topic.objects.filter(pk=id).first()
    if (topic == None):
        return redirect(reverse("forum:index"))
    else:
        return render(request, "forum/view.html", {'topic' : topic})

@login_required
def post(request):
    if (request.method == "POST"):
        topicform = TopicForm(request.POST or None)
        postnodeform = PostNodeForm(request.POST or None)
        if (topicform.is_valid() and postnodeform.is_valid()):
            topic = topicform.save(commit=False)
            postnode = postnodeform.save(commit=False)
            postnode.hidden = False
            postnode.author = request.user
            postnode.save()
            topic.head = postnode
            topic.author = request.user
            topic.save()
            return redirect(reverse('forum:index'))
        else:
            return render(request, 'forum/post.html', {'topicform' : topicform, 'postnodeform' : postnodeform})
    else:
        topicform = TopicForm()
        postnodeform = PostNodeForm()
    return render(request, 'forum/post.html', {'topicform' : topicform, 'postnodeform' : postnodeform})

@login_required
def reply(request, id):
    postnodedest = PostNode.objects.filter(pk=id).first()
    if (postnodedest == None):
        return redirect(reverse("forum:index"))
    temp = postnodedest
    if (request.method == "POST"):
        postnodeform = PostNodeForm(request.POST or None)
        if (postnodeform.is_valid()):
            postnode = postnodeform.save(commit=False)
            postnode.post = postnodedest
            postnode.author = request.user
            postnode.hidden = False
            postnode.save()
            return redirect(reverse("forum:view", args=[postnode.update_topic().pk]))
        else:
            return render(request, "forum/reply.html", {"postnodeform" : postnodeform, "postnodedest" : postnodedest})
    else:
        postnodeform = PostNodeForm()
        return render(request, "forum/reply.html", {"postnodeform" : postnodeform, "postnodedest" : postnodedest})

def upvote(request, id):
    if (not request.user.is_authenticated):
        return HttpResponseForbidden('{"error" : "login required!"}')
    else:
        if (request.method == "POST"):
            topic = get_object_or_404(Topic, pk=id)
            if (request.user in topic.downvoters.all()):
                return HttpResponseBadRequest('{"error" : "user already have downvoted"}')
            elif (request.user in topic.upvoters.all()):
                topic.upvoters.remove(request.user)
                topic.updateScore()
                return JsonResponse({'status' : 'upvote canceled'})
            else:
                topic.upvoters.add(request.user)
                topic.updateScore()
                return JsonResponse({'status' : 'upvoted'})
        else:
            return HttpResponseForbidden('{"error" : "Invalid request type!"}')

def downvote(request, id):
    if (not request.user.is_authenticated):
        return HttpResponseForbidden('{"error" : "login required!"}')
    else:
        if (request.method == "POST"):
            topic = get_object_or_404(Topic, pk=id)
            if (request.user in topic.upvoters.all()):
                return HttpResponseBadRequest('{"error" : "user already have upvoted"}')
            elif (request.user in topic.downvoters.all()):
                topic.downvoters.remove(request.user)
                topic.updateScore()
                return JsonResponse({'status' : 'downvote canceled'})
            else:
                topic.downvoters.add(request.user)
                topic.updateScore()
                return JsonResponse({'status' : 'downvoted'})
        else:
            return HttpResponseForbidden('{"error" : "Invalid request type!"}')