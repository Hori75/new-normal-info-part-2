from ckeditor.fields import RichTextField
from django import forms
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class PostNode(models.Model):
    content = RichTextField(null=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    post = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    time_creation = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False)

    def get_topic(self):
        postnode = self
        while (postnode.post != None):
            postnode = postnode.post
        return postnode.topic

    def update_topic(self):
        topic = self.get_topic()
        topic.replyCountUp()
        return topic

    def has_replies(self):
        return self.postnode_set.first() is not None

    @staticmethod
    def compare_datetime_with_now(datetime):
        time = timezone.now()

        if (datetime.year == time.year):
            if (datetime.month == time.month):
                if (datetime.day == time.day):
                    if (datetime.hour == time.hour):
                        if (datetime.minute == time.minute):
                            return str(time.second - datetime.second) + " detik lalu"
                        else:
                            return str(time.minute - datetime.minute) + " menit lalu"
                    else:
                        return str(time.hour - datetime.hour) + " jam lalu"
                else:
                    return str(time.day - datetime.day) + " hari lalu"
            else:
                return str(time.month - datetime.month) + " bulan lalu"
        else:
            return "{:02d}/{:02d}/{:04d}".format(datetime.day, datetime.month, datetime.year)

    def get_creation_datetime(self):
        return self.compare_datetime_with_now(self.time_creation)

class Topic(models.Model):
    title = models.CharField(max_length=50)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    upvoters = models.ManyToManyField(User, related_name="upvoter")
    downvoters = models.ManyToManyField(User, related_name="downvoter")
    score = models.IntegerField(default=0)
    head = models.OneToOneField(PostNode, on_delete=models.CASCADE)
    recent_update = models.DateTimeField(auto_now_add=True)
    time_creation = models.DateTimeField(auto_now_add=True)
    reply_count = models.BigIntegerField(default=0)

    def replyCountUp(self):
        self.reply_count += 1
        self.recent_update = timezone.now()
        self.save()

    def updateScore(self):
        self.score = self.upvoters.all().count() - self.downvoters.all().count()
        self.save()

    def get_recent_datetime(self):
        return PostNode.compare_datetime_with_now(self.recent_update)

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ['title']

class PostNodeForm(forms.ModelForm):
    class Meta:
        model = PostNode
        fields = ['content']
