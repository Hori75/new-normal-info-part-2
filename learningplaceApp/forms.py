from django import forms

from . import models
from .models import contactUs
from .models import TestimonialByUser

class contactUsForm(forms.ModelForm):
    class Meta:
        model = contactUs
        fields = ('name', 'email', 'subject', 'message')
        labels  = {
        'name':'Your Name', 
        'email':'Your Email', 
        'subject':'Subject', 
        'message':'Message'
        }

class TestimonialByUserForm(forms.ModelForm):
    class Meta:
        model = TestimonialByUser
        fields = ['testimonial']
        widgets = {
             'testimonial' : forms.Textarea
             (attrs={'class': 'form=control', 'id':'post-text', 'placeholder': 'Write your testimonial here...' , 'maxlength': 650}),
        }
        labels = {'testimonial' : 'Testimonial From You'}