from django.contrib import admin
from django.urls import path

from . import views
from .views import index
from .views import testimonial_section
from .views import show_testimonial

app_name = "learningplace"

urlpatterns = [
    path("", index, name="index"),
	path('testimonial/', views.testimonial_section, name='testimonial'),
    path('testimonial/api/', views.show_testimonial, name='showtestimonial')
]
