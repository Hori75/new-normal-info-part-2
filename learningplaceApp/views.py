from django.shortcuts import render
from django.shortcuts import redirect, reverse
from django.http import HttpResponse
from django.http import JsonResponse
from .models import TestimonialByUser
from .forms import TestimonialByUserForm
from .forms import contactUsForm
from .models import contactUs
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest, HttpResponseForbidden
#from django.http import HttpResponseNotFound, JsonResponse
from django.http import JsonResponse
from django.core import serializers
import json

# Create your views here.

def index(request):
	testimonials = TestimonialByUser.objects.all()
	form = contactUsForm(request.POST or None)
	if form.is_valid():
		form.save()
		form = contactUsForm()
	#print(form.errors)
	context = {
		'form': form,
		'testimonis' : testimonials
		}
	return render(request, "learningplaceApp/index.html", context)

@login_required
def testimonial_section(request):
	testimonials = TestimonialByUser.objects.all()
	form_value = TestimonialByUserForm()

	return render(request, "learningplaceApp/testimonial-page.html", {'form' : TestimonialByUserForm})
	
def show_testimonial(request):	
	testimonials = TestimonialByUser.objects.all()

	if (request.user.is_authenticated):
		if request.method == "POST":
			form_value = TestimonialByUserForm(request.POST)
			response_data = serializers.serialize('json', testimonials)

			if (form_value.is_valid()):
				obj = form_value.save(commit=False)
				obj.name = request.user.username
				obj.save()
				response_data = {"status" : "success"}
				return JsonResponse(response_data)
				
			else:
				response_data = {"status" : "fail"}
				return HttpResponseBadRequest(response_data, content_type="application/json")
		else:
			response_data = serializers.serialize('json', testimonials)
			return JsonResponse({'list': response_data})

	else:
		return HttpResponseForbidden()