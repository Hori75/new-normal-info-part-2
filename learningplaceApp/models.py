from django.db import models

# Create your models here.
class contactUs(models.Model):
    name = models.CharField(max_length=120)
    email = models.CharField(max_length=120, null=True)
    subject = models.CharField(max_length=120, blank=False, null=False)
    message = models.TextField(max_length=120, blank=False, null=False)

class TestimonialByUser(models.Model):
    name = models.CharField(max_length=350, blank=True)
    testimonial = models.TextField(max_length=650)
    date_create = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name