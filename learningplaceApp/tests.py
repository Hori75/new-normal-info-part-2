from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.test.client import RequestFactory
from django.contrib.auth.models import User, Group
from . import models
from . import forms
from . import views
from .models import contactUs
from .models import TestimonialByUser
from .forms import contactUsForm
from .forms import TestimonialByUserForm
from .views import index
from .views import testimonial_section
from .views import show_testimonial
import json
from .apps import LearningplaceappConfig
from django.apps import apps
  
# Create your tests here.

class UnitTestForLearningPlaceApp(TestCase):
    def test_response_page(self):
        response = Client().get(reverse("learningplace:index"))
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get(reverse("learningplace:index"))
        self.assertTemplateUsed(response, 'learningplaceApp/index.html')

    def test_learningplace_save_a_POST_request(self):
        obj = contactUs.objects.create(name='Daryl')
        Client().post('/' + str(obj.id), data={'name': 'Daryl'})
        jumlah = contactUs.objects.filter(name='Daryl').count()
        self.assertEqual(jumlah, 1)

    #test for forms
    def test_form_is_valid(self):
        form_contactUs = contactUsForm({
            "name": "Richard",
            "email": "richardvier@gmail.com",
            "subject": "rating",
            "message": "nice, you made an interesting website"})
        self.assertTrue(form_contactUs.is_valid())
       
    def test_form_invalid(self):
        form_contactUs = contactUsForm(data={})
        self.assertFalse(form_contactUs.is_valid())
    
    def test_form_contactUs_index(self):
        data = {'name':'Jocelyn', 
        'email':'jocelyn@gmail.com', 
        'subject':'rating', 
        'message':'8.5/10!!, kreatif banget front endnya!'
        }
        response = self.client.post('/', data=data)
        model = contactUs.objects.all().first()
        self.assertEqual(model.name, "Jocelyn")
        self.assertEqual(model.email, "jocelyn@gmail.com")
        self.assertEqual(model.subject, "rating")
        self.assertEqual(model.message, "8.5/10!!, kreatif banget front endnya!")

    #test for models
    def test_instance_created(self):
        self.assertEqual(contactUs.objects.count(), 0)
    
    #test for views 
    def test_setup(self):
        self.client = Client()
        self.product_create_view = reverse("learningplace:index")

    def test_GET_index(self):
        self.index = reverse("learningplace:index")
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'learningplaceApp/index.html')

class UnitTestForTestimonialByUserApp(TestCase):
    #setUp agar bisa ditest setelah user ter-authenticate
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='Daryl', email="daryl@works.com", password='789halokamu')
        self.pemberitestimonial = TestimonialByUser.objects.create(
            name = "Daryl",
            testimonial = "asik keren banget",
            date_create = "2 Jan"
        )
        
    def test_response_page(self):
        response = Client().get(reverse("learningplace:testimonial"))
        self.assertEqual(response.status_code, 302)
    
    def test_testimoni_post_data_empty(self):
        request = self.factory.post(reverse('learningplace:testimonial'))
        request.user = self.user
        response = testimonial_section(request)
        self.assertEqual(response.status_code, 200)

    # test for models
    def test_django_model_can_create_new_testimoni(self):
        new_testimoni = TestimonialByUser.objects.create(testimonial = 'nice website, tetep semangat, bentar lagi libur!!')
        counting_all_available_todo = TestimonialByUser.objects.all().count()
        self.assertEqual(counting_all_available_todo, 2)

    def test_instance_success_created(self):
        self.assertEqual(TestimonialByUser.objects.count(), 1)
    
    def test_str_in_model(self):
        self.assertEqual(str(self.pemberitestimonial), "Daryl")
    
    # test for forms
    def test_form_is_valid(self):
        form_testimonial = TestimonialByUserForm(
            data={
                'name': "Daryl",
                'testimonial': "nice!",
                'date_create': "2 Jan",
            }
        )
        self.assertTrue(form_testimonial.is_valid())
    
    def test_form_invalid(self):
        form_testimonial = TestimonialByUserForm(
            data={}
        )
        self.assertFalse(form_testimonial.is_valid())
    
    # test for views
    def test_response_testimonial_page(self):
        new_testimoni = TestimonialByUser.objects.create(testimonial = 'nice website, tetep semangat, bentar lagi libur!!')
        response = Client().get(reverse("learningplace:showtestimonial"))
        self.assertEqual(response.status_code, 403)

    def test_show_testimonials_after_logged_in(self):
        request = self.factory.get(reverse("learningplace:showtestimonial"))
        request.user = self.user
        response = show_testimonial(request)
        jsonresponse = json.loads(response.content)
        objList = json.loads(jsonresponse['list'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(objList[0]['fields']['testimonial'], 'asik keren banget')
    
    def test_post_testimonial_is_failed(self):
        request = self.factory.post(reverse("learningplace:showtestimonial"))
        request.user = self.user
        response = show_testimonial(request)
        self.assertEqual(response.status_code, 400)
    
    def test_post_testimonial_is_success(self):
        request = self.factory.post(reverse("learningplace:showtestimonial"), {"testimonial":"asik keren banget"})
        request.user = self.user
        response = show_testimonial(request)
        self.assertEqual(response.status_code, 200)
    
class TestApp(TestCase):
    def test_app_learningplace(self):
        self.assertEqual(LearningplaceappConfig.name, 'learningplaceApp')
        self.assertEqual(apps.get_app_config('learningplaceApp').name, 'learningplaceApp')



    