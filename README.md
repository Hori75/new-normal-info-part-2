# New Normal Info

![Pipeline](https://gitlab.com/hori75/new-normal-info/badges/master/pipeline.svg)
![coverage](https://gitlab.com/hori75/new-normal-info/badges/master/coverage.svg)

Sudah beberapa bulan semenjak awal dari respons darurat pandemi dan aturan pembatasan sosial 
berskala besar telah diperlonggar. Namun kita tidak dapat meremehkan dampak dari pandemi ini. 
Dampak khusus untuk pelajar adalah penutupan sekolah dan kegiatan pembelajaran 
harus dilakukan dari rumah. Terdapat sebagian dari pelajar yang tidak mempunyai koneksi internet 
atau tak dapat berkonsentrasi belajar di rumah. Mereka pun kebingungan karena tidak mengetahui 
tempat alternatif agar kegiatan belajar dapat dilakukan secara maksimal.

Aplikasi web ini dibuat untuk menyalurkan informasi yang dibutuhkan dalam new normal
seperti tempat belajar yang aman selama pandemi, daftar rumah sakit rujukan, serta 
forum untuk komunikasi antar pengguna dan penyaluran berita terbaru mengenai pandemi ini.

Link Heroku app: https://newnormalinfo.herokuapp.com

## Fitur yang akan diimplementasikan

 - Home (main page)
 - Daftar Rumah Sakit Rujukan
 - Tempat Belajar yang aman selama pandemi
 - Forum

## Daftar anggota kelompok

 - (1906307132) Muchlisah Lusiambali 
 - (1906350521) William
 - (1906350976) Mochammed Daffa El Ghifari
 - (1906305455) Alfina Megasiwi

## Catatan tambahan

Pengaturan project ini dibuat berdasarkan template dari Kak Sage
(https://github.com/laymonage/django-template-heroku)
